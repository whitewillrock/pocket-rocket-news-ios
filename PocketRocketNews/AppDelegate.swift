//
//  AppDelegate.swift
//  PocketRocketNews
//
//  Created by Алексей on 19/06/2017.
//  Copyright © 2017 Алексей. All rights reserved.
//

import UIKit
import MagicalRecord

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        configureCoreData()
        configureMainView()
        
        // Override point for customization after application launch.
        return true
    }
    
    fileprivate func configureCoreData(){
        MagicalRecord.cleanUp()
        MagicalRecord.setupCoreDataStack(withStoreNamed: "PocketRocketNews")
    }
    
    fileprivate func configureMainView(){
        window = UIWindow(frame: UIScreen.main.bounds)
        let mainView = BaseTabViewController()
        window?.rootViewController = mainView
        window?.makeKeyAndVisible()
    }
}

