//
//  FeedItem.swift
//  PocketRocketNews
//
//  Created by Алексей on 16.03.17.
//  Copyright © 2017 Алексей. All rights reserved.
//

import Foundation
import AEXML

struct FeedItem {
    let title: String
    let text: String
    let pubDate: Date
    let image: URL?
    let link: URL?
    
    init(from item: AEXMLElement) {        
        self.title = item["title"].string
        self.text = item["description"].string.withoutHTML
        self.pubDate = item["pubDate"].string.rssDate()
        self.image = item["description"].string.rssImage()
        self.link = URL(string: item["link"].string)        
    }
}
