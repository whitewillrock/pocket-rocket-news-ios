//
//  BaseTabViewController.swift
//  PocketRocketNews
//
//  Created by Алексей on 19/06/2017.
//  Copyright © 2017 Алексей. All rights reserved.
//

import UIKit

class BaseTabViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        configureTabbar()
    }
    
    private func configureTabbar(){
        let listFeed = listFeedsVC()
        let sourceFeed = sourceFeedsVC()
        
        viewControllers = [listFeed, sourceFeed]
        
        listFeed.tabBarItem = UITabBarItem(title: "Лента", image: #imageLiteral(resourceName: "icon_list"), selectedImage: #imageLiteral(resourceName: "icon_list_selected"))
        sourceFeed.tabBarItem = UITabBarItem(title: "Источники", image: #imageLiteral(resourceName: "icon_source"), selectedImage: #imageLiteral(resourceName: "icon_source_selected"))
    }
    
    private func listFeedsVC() -> UIViewController{
        let vc = ListFeedsVC()
        let navView = UINavigationController(rootViewController: vc)
        return navView
    }
    
    private func sourceFeedsVC() -> UIViewController{
        let vc = SourceFeedsVC()
        let navView = UINavigationController(rootViewController: vc)
        return navView
    }
}

