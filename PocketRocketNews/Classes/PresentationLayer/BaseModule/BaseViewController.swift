//
//  BaseViewController.swift
//  PocketRocketNews
//
//  Created by Алексей on 19/06/2017.
//  Copyright © 2017 Алексей. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {

    private var emptyView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        configureEmptyView()        
    }
    
    func configureUI(){
        log.error("configureUI must bee override")
        fatalError()
    }

    func showAlert(with error: String){
        let alert = UIAlertController(title: "Rss источник", message: error, preferredStyle: .alert)

        let doneButton = UIAlertAction(title: "ОК", style: .default) { (alertAction) in
            alert.dismiss(animated: true, completion: nil)
        }
        alert.addAction(doneButton)
        present(alert, animated: true, completion: nil)
    }
    
    func emptyView(isShow: Bool = false){
        emptyView.isHidden = !(isShow)
    }
    
    private func configureEmptyView(){
        emptyView = UIView(frame: CGRect.zero)
        view.addSubview(emptyView)
        emptyView.snp.makeConstraints { (maker) in
            maker.edges.equalToSuperview().inset(UIEdgeInsetsMake(0, 0, 0, 0))
        }
        
        let emptyIcon = UIImageView()
        emptyView.addSubview(emptyIcon)
        emptyIcon.snp.makeConstraints { (maker) in
            maker.size.equalTo(CGSize(width: 128, height: 128))
            maker.center.equalToSuperview()
        }
        
        let emptyLabel = UILabel(frame: CGRect.zero)
        emptyView.addSubview(emptyLabel)
        emptyLabel.snp.makeConstraints { (maker) in
            maker.top.equalTo(emptyIcon.snp.bottom).offset(12)
            maker.centerX.equalToSuperview()
        }

        emptyView.isHidden = true
        emptyView.backgroundColor = UIColor(hexString: "#EFEFF4")
        
        emptyIcon.image = #imageLiteral(resourceName: "icon_add_list")
        emptyIcon.contentMode = .scaleAspectFit
        
        emptyLabel.textColor = UIColor(hexString: "#9A9A9B")
        emptyLabel.text = "Чтобы добавить новый источник\nнажмите \"+\" в источниках"
        emptyLabel.numberOfLines = 2
    }
}
