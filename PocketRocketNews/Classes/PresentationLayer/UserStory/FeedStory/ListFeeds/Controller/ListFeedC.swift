//
//  ListFeedC.swift
//  PocketRocketNews
//
//  Created by Алексей on 19/06/2017.
//  Copyright © 2017 Алексей. All rights reserved.
//

import Foundation
import CoreData

protocol ListFeedCDelegate: class {
    func performRefreshFetch()
}

final class ListFeedC{
    weak var delegate: ListFeedCDelegate?
    
    fileprivate let sourceService = SourceService()
    
    init() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.refreshFRC(_:)), name: NSNotification.Name.NSManagedObjectContextObjectsDidChange, object: NSManagedObjectContext.mr_default())
    }
    
    func refreshLinks(){
        sourceService.refresh()
    }
    
    @objc
    func refreshFRC(_ notif: NSNotification){
        if let set = notif.userInfo?[NSRefreshedObjectsKey] as? NSSet{
            if set.allObjects.flatMap({
                return $0 as? SourceCD
            }).count > 0{
                self.delegate?.performRefreshFetch()
            }
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}
