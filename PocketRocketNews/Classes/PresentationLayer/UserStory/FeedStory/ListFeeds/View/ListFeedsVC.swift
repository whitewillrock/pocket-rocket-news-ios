//
//  ListFeedsVC
//  PocketRocketNews
//
//  Created by Алексей on 19/06/2017.
//  Copyright © 2017 Алексей. All rights reserved.
//

import UIKit
import SnapKit
import MagicalRecord

class ListFeedsVC: BaseViewController {

    var tableView: UITableView!
    var frc: NSFetchedResultsController<NSFetchRequestResult>!
    let detailVC = DetailFeedVC()
    
    fileprivate let controller = ListFeedC()
    fileprivate var state:State = .all
    
    enum State {
        case all, favorite
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        controller.delegate = self
        configureFRC()
    }
    
    override func configureUI() {
        configureTableView()
        configureNavigationController()
    }
    
    private func configureTableView(){
        tableView = UITableView(frame: CGRect.zero, style: .grouped)
        view.addSubview(tableView)

        tableView.snp.makeConstraints { (maker) in
            maker.edges.equalToSuperview().inset(UIEdgeInsets.zero)
        }
        tableView.contentInset = UIEdgeInsetsMake(20, 0, 0, 0)
        tableView.contentOffset = CGPoint(x: 0, y: -20)
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = ListFeedCell.kEstimateHeight
        
        tableView.register(cellType: ListFeedCell.self)
    }
    
    private func configureFRC(){
        switch state {
        case .all:
            configureAllFRC()
            
        case .favorite:
            configureFavoriteFRC()
        }
        emptyView(isShow: frc.fetchedObjects?.count == 0)
    }
    
    private func configureAllFRC(){
        frc = FeedItemCD.mr_fetchAllGrouped(by: "source.name",
                                            with: nil,
                                            sortedBy: "source.isFavorite,source.date,date",
                                            ascending: false,
                                            delegate: self)
    }
    
    private func configureFavoriteFRC(){
        let predicate = NSPredicate(format: "source.isFavorite = true")
        frc = FeedItemCD.mr_fetchAllGrouped(by: "source.name",
                                            with: predicate,
                                            sortedBy: "source.date,date",
                                            ascending: false,
                                            delegate: self)

    }
    
    fileprivate func refreshFRC(){
        NSFetchedResultsController<NSFetchRequestResult>.deleteCache(withName: nil)
        do{
            try frc.performFetch()
        }
        catch{
            log.error("cannot update frc")
        }
        emptyView(isShow: frc.fetchedObjects?.count == 0)
        tableView.reloadData()
    }
    
    private func configureNavigationController(){
        navigationController?.navigationBar.topItem?.title = "Новостная лента"
        let refreshButton = UIBarButtonItem(barButtonSystemItem: .refresh, target: self, action: #selector(self.didTapRefresh(_:)))
        navigationItem.rightBarButtonItem = refreshButton

        let favoriteButton = UIBarButtonItem(image: #imageLiteral(resourceName: "icon_heart_small"), style: .done, target: self, action: #selector(self.didTapFavorite(_:)))
        navigationItem.leftBarButtonItem = favoriteButton
    }
    
    @objc
    private func didTapRefresh(_ sender: UIBarButtonItem){
        controller.refreshLinks()
    }
    
    @objc
    private func didTapFavorite(_ sender: UIBarButtonItem){
        if state == .all{
            state = .favorite
            sender.image = #imageLiteral(resourceName: "icon_heart_small_filled")
        }else{
            state = .all
            sender.image = #imageLiteral(resourceName: "icon_heart_small")
        }
        
        configureFRC()
        refreshFRC()
    }
}

extension ListFeedsVC: ListFeedCDelegate{
    func performRefreshFetch() {
        refreshFRC()
    }
}
