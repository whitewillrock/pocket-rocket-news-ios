//
//  TableViewListFeedsVC.swift
//  PocketRocketNews
//
//  Created by Алексей on 19/06/2017.
//  Copyright © 2017 Алексей. All rights reserved.
//

import UIKit

extension ListFeedsVC: UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return frc.sections?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let sectionData = frc.sections?[section]{
            return sectionData.numberOfObjects
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(for: indexPath, cellType: ListFeedCell.self)
        configure(cell, at: indexPath)
        return cell
    }
    
    func configure(_ cell: ListFeedCell, at indexPath: IndexPath){
        if let object = frc.object(at: indexPath) as? FeedItemCD {
            cell.configure(icon: URL(string: object.image ?? ""),
                           name: object.title ?? "",
                           subName: object.text ?? "")
        }
    }    
}

extension ListFeedsVC: UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if let feed = frc.object(at: indexPath) as? FeedItemCD{
            if let link = feed.link{
                detailVC.showDetailFeed(with: link, from: self)
                return
            }
            
            let name = "источник"
            showAlert(with: "\(feed.source?.name ?? name) не содержит адреса ")
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return SourceFeedHeaderView.height
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if let source = (frc.sections?[section].objects?.first as? FeedItemCD)?.source{
            let frame = CGRect(x: 0, y: 0, width: tableView.frame.width, height: SourceFeedHeaderView.height)
            return SourceFeedHeaderView(frame: frame, title: source.name ?? "", isFavorite: source.isFavorite)
        }
        
        return nil
    }
}
