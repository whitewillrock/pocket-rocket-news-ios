//
//  SourceFeedHeaderView.swift
//  PocketRocketNews
//
//  Created by Алексей on 22/06/2017.
//  Copyright © 2017 Алексей. All rights reserved.
//

import UIKit


class SourceFeedHeaderView: UIView {
    static let height: CGFloat = 30.0

    private var title: UILabel!
    private var favorite: UIImageView!
    
    init(frame: CGRect, title: String, isFavorite: Bool) {
        super.init(frame: frame)
        configureUI()
        
        self.title.text = title
        favorite.isHidden = !(isFavorite)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        configureUI()
    }
    
    private func configureUI(){
        configureTitle()
        configureFavorite()
    }
    
    private func configureTitle(){
        title = UILabel()
        self.addSubview(title)
        title.snp.makeConstraints { (maker) in
            maker.top.greaterThanOrEqualToSuperview().inset(4)
            maker.bottom.equalToSuperview().inset(4)
            maker.leading.equalToSuperview().inset(8)
        }
        title.font = UIFont.boldSystemFont(ofSize: 14)
    }
    
    private func configureFavorite(){
        favorite = UIImageView(frame: CGRect.zero)
        self.addSubview(favorite)
        favorite.snp.makeConstraints { (maker) in
            maker.trailing.equalToSuperview().inset(16)
            maker.leading.equalTo(title.snp.trailing).inset(-16)
            maker.size.equalTo(CGSize(width: 16, height: 16))
            maker.centerY.equalTo(title.snp.centerY)
        }
        favorite.image = #imageLiteral(resourceName: "icon_heart")
    }
}
