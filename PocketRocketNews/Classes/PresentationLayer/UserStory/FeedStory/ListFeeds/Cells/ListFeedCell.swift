//
//  ListFeedCell.swift
//  PocketRocketNews
//
//  Created by Алексей on 19/06/2017.
//  Copyright © 2017 Алексей. All rights reserved.
//

import UIKit
import Reusable
import SnapKit
import SDWebImage

class ListFeedCell: BaseTableViewCell, Reusable {
    static let kEstimateHeight: CGFloat = 80.0
    
    private var icon: UIImageView!
    private var title: UILabel!
    private var subTitle: UILabel!
    
    func configure(icon: URL?, name: String, subName: String){
        self.icon.sd_setImage(with: icon, placeholderImage: #imageLiteral(resourceName: "icon_no_image"))
        self.title.text = name
        self.subTitle.text = subName
    }
    
    override func configureUI() {
        configureIcon()
        configureTitle()
        configureSubTitle()
    }
    
    private func configureIcon(){
        icon = UIImageView(frame: CGRect.zero)
        self.addSubview(icon)
        
        icon.snp.makeConstraints { (maker) in
            maker.top.leading.equalTo(8)
            maker.size.equalTo(CGSize(width: 52, height: 52))
        }
        icon.contentMode = .scaleAspectFit
        icon.image = #imageLiteral(resourceName: "icon_no_image")
    }
    
    private func configureTitle(){
        title = UILabel(frame: CGRect.zero)
        self.addSubview(title)
        
        title.snp.makeConstraints { (maker) in
            maker.top.equalTo(icon.snp.top)
            maker.leading.equalTo(icon.snp.trailing).offset(12)
            maker.trailing.equalToSuperview().offset(-8)
            maker.height.greaterThanOrEqualTo(20)
        }
        title.font = UIFont.systemFont(ofSize: 14)
        title.numberOfLines = 2
    }
    
    private func configureSubTitle(){
        subTitle = UILabel(frame: CGRect.zero)
        self.addSubview(subTitle)
        subTitle.snp.makeConstraints { (maker) in
            maker.top.equalTo(title.snp.bottom).offset(0)
            maker.leading.equalTo(title.snp.leading).offset(0)
            maker.trailing.equalTo(title.snp.trailing).offset(0)
            maker.bottom.equalToSuperview().offset(-8)
            maker.height.greaterThanOrEqualTo(20)
        }
        subTitle.font = UIFont.systemFont(ofSize: 12, weight: UIFontWeightThin)
        subTitle.numberOfLines = 3
    }
}

extension ConstraintMaker {
    public func aspectRatio(_ x: Int, by y: Int, self instance: ConstraintView) {
        self.width.equalTo(instance.snp.height).multipliedBy(x / y)
    }
}
