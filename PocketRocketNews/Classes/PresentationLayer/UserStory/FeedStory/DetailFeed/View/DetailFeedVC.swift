//
//  DetailFeedVC.swift
//  PocketRocketNews
//
//  Created by Алексей on 22/06/2017.
//  Copyright © 2017 Алексей. All rights reserved.
//

import Foundation
import SafariServices

class DetailFeedVC{
    func showDetailFeed(with link: String, from vc: UIViewController){
        if let link = URL(string: link){
            let safariVC = SFSafariViewController(url: link, entersReaderIfAvailable: true)
            vc.present(safariVC, animated: true, completion: nil)
        }
    }
}
