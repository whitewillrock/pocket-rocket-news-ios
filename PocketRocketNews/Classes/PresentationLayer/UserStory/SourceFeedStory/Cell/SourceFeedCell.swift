//
//  SourceFeedCell.swift
//  PocketRocketNews
//
//  Created by Алексей on 19/06/2017.
//  Copyright © 2017 Алексей. All rights reserved.
//

import UIKit
import Reusable
import SnapKit

class SourceFeedCell: BaseTableViewCell, Reusable {
    static let height: CGFloat = 40.0
    
    private var title: UILabel!
    private var favorite: UIImageView!
    
    func configure(with name: String, isFavorite: Bool){
        title.text = name
        favorite.isHidden = !(isFavorite)
    }
    
    override func configureUI() {
        configureTitle()
        configureFavorite()
    }
    
    private func configureTitle(){
        title = UILabel()
        self.addSubview(title)
        title.snp.makeConstraints { (maker) in
            maker.top.bottom.equalToSuperview().inset(8)
            maker.leading.equalToSuperview().inset(44)
        }
        
        title.numberOfLines = 2
    }
    
    private func configureFavorite(){
        favorite = UIImageView(frame: CGRect.zero)
        self.addSubview(favorite)
        favorite.snp.makeConstraints { (maker) in
            maker.trailing.equalToSuperview().inset(16)
            maker.leading.equalTo(title.snp.trailing).inset(-16)
            maker.size.equalTo(CGSize(width: 16, height: 16))
            maker.centerY.equalToSuperview()
        }
        favorite.image = #imageLiteral(resourceName: "icon_heart")
    }
}
