//
//  TableViewSourceFeedsVC.swift
//  PocketRocketNews
//
//  Created by Алексей on 19/06/2017.
//  Copyright © 2017 Алексей. All rights reserved.
//

import UIKit

extension SourceFeedsVC: UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return frc.sections?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let sectionData = frc.sections?[section]{
            return sectionData.numberOfObjects
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(for: indexPath, cellType: SourceFeedCell.self)
        configure(cell, at: indexPath)
        return cell
    }
    
    func configure(_ cell: SourceFeedCell, at indexPath: IndexPath){
        if let object = frc.object(at: indexPath) as? SourceCD {
            if let name = object.name{
                cell.configure(with: name, isFavorite: object.isFavorite)
                return
            }
            
            if let link = object.link{
                cell.configure(with: link, isFavorite: object.isFavorite)
                return
            }
        }
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        guard let object = frc.object(at: indexPath) as? SourceCD else {
            log.warning("object is not SourceCD")
            return
        }
        controller.remove(object)
    }
}

extension SourceFeedsVC: UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return SourceFeedCell.height
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        return .delete
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        guard let object = frc.object(at: indexPath) as? SourceCD else {
            log.warning("object is not SourceCD")
            return
        }
        controller.changeFavorite(object)
    }
}
