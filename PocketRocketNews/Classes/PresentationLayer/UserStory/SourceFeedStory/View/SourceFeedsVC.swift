//
//  SourceFeedsVC.swift
//  PocketRocketNews
//
//  Created by Алексей on 19/06/2017.
//  Copyright © 2017 Алексей. All rights reserved.
//

import UIKit
import MagicalRecord

class SourceFeedsVC: BaseViewController {
    var tableView: UITableView!
    var frc: NSFetchedResultsController<NSFetchRequestResult>!
    
    let controller = SourceFeedsC()

    override func viewDidLoad() {
        super.viewDidLoad()

        controller.delegate = self
        configureFRC()
    }
    
    override func configureUI() {
        configureTableView()
        configureNavigationController()
    }
    
    func checkEditing(){
        if frc.fetchedObjects?.count == 0 && tableView.isEditing == true{
            tableView.setEditing(false, animated: false)
        }
    }
    
    private func configureFRC(){
        frc = SourceCD.mr_fetchAllSorted(by: "isFavorite,date", ascending: false, with: nil, groupBy: nil, delegate: self)
        emptyView(isShow: frc.fetchedObjects?.count == 0)
    }
    
    private func configureTableView(){
        tableView = UITableView(frame: CGRect.zero, style: .plain)
        view.addSubview(tableView)
        
        tableView.snp.makeConstraints { (maker) in
            maker.edges.equalToSuperview().inset(UIEdgeInsets.zero)
        }
        
        tableView.tableFooterView = UIView()

        tableView.delegate = self
        tableView.dataSource = self

        tableView.register(cellType: SourceFeedCell.self)
    }
    
    private func configureNavigationController(){
        navigationController?.navigationBar.topItem?.title = "Новостные источники"
        
        let addButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(self.didTapAdd(_:)))
        let editButton = UIBarButtonItem(image: #imageLiteral(resourceName: "icon_edit"), style: .done, target: self, action: #selector(self.didTapEdit(_:)))
        navigationItem.rightBarButtonItems = [addButton, editButton]
    }

    @objc
    private func didTapAdd(_ sender: UIBarButtonItem) {
        let alert = UIAlertController(title: "Rss источник", message: "Адрес прямой ссылки", preferredStyle: .alert)
        alert.addTextField { (textfield) in
            textfield.placeholder = "https://www.kinopoisk.ru/news.rss"
        }
        
        let cancellButton = UIAlertAction(title: "Отмена", style: .cancel) { (alertAction) in
            alert.dismiss(animated: true, completion: nil)
        }
        let doneButton = UIAlertAction(title: "Добавить", style: .default) { (alertAction) in
            self.controller.addLink(link: alert.textFields?.first?.text ?? "")
        }
        alert.addAction(cancellButton)
        alert.addAction(doneButton)
        present(alert, animated: true, completion: nil)
    }
    
    @objc
    private func didTapEdit(_ sender: UIBarButtonItem){
        if let count = frc.fetchedObjects?.count{
            if count > 0 && tableView.isEditing == false{
                tableView.setEditing(true, animated: true)
                return
            }
        }
        
        tableView.setEditing(false, animated: true)
    }
}

extension SourceFeedsVC: SourceFeedsCDelegate{
    func didFailed(error: String) {
        showAlert(with: error)
    }
}
