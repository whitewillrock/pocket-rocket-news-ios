//
//  SourceFeedC.swift
//  PocketRocketNews
//
//  Created by Алексей on 19/06/2017.
//  Copyright © 2017 Алексей. All rights reserved.
//

import Foundation

protocol SourceFeedsCDelegate: class {
    func didFailed(error: String)
}

final class SourceFeedsC{
    weak var delegate: SourceFeedsCDelegate?
    
    fileprivate let sourceService = SourceService()
    init() {
        sourceService.delegate = self
    }
    
    func addLink(link: String){
        if link.validateLink(){
            sourceService.add(link: link)
            return
        }
        delegate?.didFailed(error: "Адрес не является url ссылкой")
    }
    
    func remove(_ object: SourceCD){
        sourceService.remove(object)
    }
    
    func changeFavorite(_ object: SourceCD){
        sourceService.changeFavorire(object)
    }
}

extension SourceFeedsC: SourceServiceDelegate{
    func sourceService(didFailed error: DataServiceError) {
        switch error {
        case .connectionFailed:
            delegate?.didFailed(error: "Не удалось подключиться к источнику новостей\n или недоступен интернет ")
        case .parseFailed(let message), .sourceError(let message):
            delegate?.didFailed(error: message)
        }
    }
}
