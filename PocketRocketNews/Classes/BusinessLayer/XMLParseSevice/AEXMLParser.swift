//
//  SWXMLParser.swift
//  PocketRocketNews
//
//  Created by Алексей on 18.03.17.
//  Copyright © 2017 Алексей. All rights reserved.
//

import Foundation
import AEXML

final class AEXMLParser: XMLParseService {
    func source(link: String, object: Data?,
                complete: (_ source: Source?) -> Void,
                failed: @escaping (_ error: DataServiceError) -> Void) {
        guard let data = object else {
            failed(.parseFailed("Фиды отсуствуют"))
            return
        }
        
        let options = AEXMLOptions()
        do {
            let xmlDoc = try AEXMLDocument(xml: data, options: options)
            guard let name = xmlDoc.root["channel"]["title"].value else {
                failed(.parseFailed("не удалось преобразовать список фидом"))
                return
            }
            let feedItems = items(from: xmlDoc.root["channel"]["item"].all)
            let source = Source(name: name, link: URL(string: link), date: Date(), items: feedItems, isFavorite: false)
            complete(source)
            
        } catch {
            failed(.parseFailed("не удалось отобразить ленту"))
            return 
        }
    }
    
    fileprivate func items(from object: [AEXMLElement]?) -> [FeedItem] {
        guard let items = object else {
            log.warning("фиды не найдены")
            return []
        }
        
        return items.map({
            FeedItem(from: $0)
        })
    }
}

