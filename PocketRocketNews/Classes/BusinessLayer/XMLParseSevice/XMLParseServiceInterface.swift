//
//  XMLParseServiceInterface.swift
//  PocketRocketNews
//
//  Created by Алексей on 19/06/2017.
//  Copyright © 2017 Алексей. All rights reserved.
//

import Foundation

protocol XMLParseService {
    func source(link: String, object: Data?,
                complete: (_ source: Source?) -> Void,
                failed: @escaping (_ error: DataServiceError) -> Void)
}

struct XMLParser{
    static func fabrick() -> XMLParseService{
        return AEXMLParser()
    }
}

