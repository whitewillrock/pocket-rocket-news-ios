//
//  SourceServiceRequest.swift
//  PocketRocketNews
//
//  Created by Алексей on 19/06/2017.
//  Copyright © 2017 Алексей. All rights reserved.
//

import Foundation


final class SourceServiceRequest{
    fileprivate let parser = XMLParser.fabrick()
    fileprivate let network = NetworkManager()
    
    func getFeedList(at path: String,
                     complete: @escaping (_ source: Source?) -> Void,
                     failed: @escaping (_ error: DataServiceError) -> Void){
        network.request(at: path, with: .post, and: [:], complete: { (data) in
            
            self.parser.source(link: path, object: data, complete: { (source) in
                complete(source)
            }, failed: { (error) in
                failed(error)
            })
            
        }) { (error) in
            failed(error)
        }
    }
}
