//
//  NetworkManager.swift
//  PocketRocketNews
//
//  Created by Алексей on 16.03.17.
//  Copyright © 2017 Алексей. All rights reserved.
//

import UIKit
import Alamofire

final class NetworkManager {
    func request(at path: String, with method: HTTPMethod,
                       and params: Parameters,
                       complete: @escaping (_ result: Data?) -> Void,
                       failedBlock failed: @escaping (_ error: DataServiceError) -> Void) {

        var headers = HTTPHeaders()
        headers["Content-Type"] = "application/rss+xml"
        Alamofire.request(path, method: method, parameters: params).responseData { (response) in
            switch response.result{
            case .failure(_):
                failed(.connectionFailed)
                
            case .success(_):
                complete(response.data)
            }
        }
    }
}
