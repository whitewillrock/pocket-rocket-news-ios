//
//  Logger.swift
//  PocketRocketNews
//
//  Created by Алексей on 19/06/2017.
//  Copyright © 2017 Алексей. All rights reserved.
//

import Foundation
import XCGLogger


let log: XCGLogger = {
    // Setup XCGLogger
    let log = XCGLogger.default
    
    let emojiLogFormatter = PrePostFixLogFormatter()
    emojiLogFormatter.apply(prefix: "🗯 ", postfix: " 🗯", to: .verbose)
    emojiLogFormatter.apply(prefix: "🔹 ", postfix: " 🔹", to: .debug)
    emojiLogFormatter.apply(prefix: "ℹ️ ", postfix: " ℹ️", to: .info)
    emojiLogFormatter.apply(prefix: "⚠️ ", postfix: " ⚠️", to: .warning)
    emojiLogFormatter.apply(prefix: "‼️ ", postfix: " ‼️", to: .error)
    emojiLogFormatter.apply(prefix: "💣 ", postfix: " 💣", to: .severe)
    log.formatters = [emojiLogFormatter]
    
    return log
}()
