//
//  String+RemoveHTML.swift
//  PocketRocketNews
//
//  Created by Алексей on 22/06/2017.
//  Copyright © 2017 Алексей. All rights reserved.
//

import Foundation

extension String{
    var withoutHTML: String{
        do{
            let regex =  "<[^>]+>"
            let expr = try NSRegularExpression(pattern: regex, options: NSRegularExpression.Options.caseInsensitive)
            return expr.stringByReplacingMatches(in: self, options: [], range: NSMakeRange(0, self.characters.count), withTemplate: "")
        }catch{
            return ""
        }
    }
}

