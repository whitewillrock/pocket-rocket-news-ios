//
//  String+ValidateURL.swift
//  PocketRocketNews
//
//  Created by Алексей on 22/06/2017.
//  Copyright © 2017 Алексей. All rights reserved.
//

import Foundation

extension String{
    func validateLink() -> Bool{
        let types: NSTextCheckingResult.CheckingType = .link
        let detector = try? NSDataDetector(types: types.rawValue)
        if (detector?.firstMatch(in: self, options: .reportCompletion, range: NSMakeRange(0, self.characters.count))) != nil{
            return true
        }
        
        return false
    }
}
