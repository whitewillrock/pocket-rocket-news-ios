//
//  String+RSSDate.swift
//  PocketRocketNews
//
//  Created by Алексей on 19/06/2017.
//  Copyright © 2017 Алексей. All rights reserved.
//

import Foundation

extension String{
    func rssDate() -> Date{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEE, dd MMM yyyy HH:mm:ss zzz"
        if let date = dateFormatter.date(from: self){
            return date
        }
        
        log.warning("не удалось преобразовать дату \(self)")
        return Date()
    }
}
