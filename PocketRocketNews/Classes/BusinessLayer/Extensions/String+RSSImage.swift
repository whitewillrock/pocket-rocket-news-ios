//
//  String+RSSImage.swift
//  PocketRocketNews
//
//  Created by Алексей on 19/06/2017.
//  Copyright © 2017 Алексей. All rights reserved.
//

import Foundation

extension String{
    func rssImage() -> URL? {
        let pattern = "<img.*?src=\"([^\"]*)\""
        do {
            let regex = try NSRegularExpression(pattern: pattern, options: .caseInsensitive)
            guard let match = regex.firstMatch(in: self, options: .withoutAnchoringBounds, range: NSMakeRange(0, self.characters.count)) else {
                return nil
            }
            let imageStr = (self as NSString).substring(with: match.rangeAt(1))
            return URL(string: imageStr)
            
        } catch {
            log.warning("не удалось преобразовать строку \(self)")            
            return nil
        }
    }
}
