//
//  DataService.swift
//  PocketRocketNews
//
//  Created by Алексей on 19/06/2017.
//  Copyright © 2017 Алексей. All rights reserved.
//

import Foundation

enum DataServiceError {
    case connectionFailed
    case parseFailed(String)
    case sourceError(String)
}
