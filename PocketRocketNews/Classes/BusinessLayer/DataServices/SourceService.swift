//
//  SourceService.swift
//  PocketRocketNews
//
//  Created by Алексей on 19/06/2017.
//  Copyright © 2017 Алексей. All rights reserved.
//

import Foundation

protocol SourceServiceDelegate: class {
    func sourceService(didFailed error: DataServiceError)
}

class SourceService{
    weak var delegate: SourceServiceDelegate?
    
    fileprivate lazy var sourceServiceData = SourceServiceData()
    fileprivate lazy var sourceSerivceRequest = SourceServiceRequest()
        
    func add(link: String){
        add(at: link)
    }
    
    func remove(_ object: SourceCD){
        sourceServiceData.remove(object)
    }
    
    func refresh(){
        refreshAllLinks()
    }
    
    func changeFavorire(_ sourceCD: SourceCD){
        sourceServiceData.changeFavorite(sourceCD)
    }
    
    fileprivate func add(at link: String){
        sourceServiceData.add(link: link, complete: { [weak self] in
            self?.getFeedList(at: link)
            
        }) { [weak self] (error) in
            self?.delegate?.sourceService(didFailed: error)
        }
    }
    
    fileprivate func getFeedList(at link: String){
        sourceSerivceRequest.getFeedList(at: link, complete: { [weak self] (source) in
            self?.saveFeedList(source: source)
        }) { [weak self] (error) in
            self?.delegate?.sourceService(didFailed: error)
        }
    }
    
    fileprivate func saveFeedList(source: Source?){
        if let source = source{
            sourceServiceData.save(source)
        }
    }
    
    fileprivate func refreshAllLinks(){
        guard let links = sourceServiceData.links() else {
            log.warning("Source Links not found")
            return
        }
        
        _ = links.map({
            self.getFeedList(at: $0.link ?? "")
        })
    }
}
