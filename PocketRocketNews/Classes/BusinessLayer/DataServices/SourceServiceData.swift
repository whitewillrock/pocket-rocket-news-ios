//
//  SourceService.swift
//  PocketRocketNews
//
//  Created by Алексей on 19/06/2017.
//  Copyright © 2017 Алексей. All rights reserved.
//

import Foundation
import MagicalRecord

class SourceServiceData {
    func save(_ source: Source) {
        MagicalRecord.save({ (context) in
            let sourceCD = self.sourceBy("link", with: source.link?.absoluteString ?? "" , in: context)
            sourceCD?.name = source.name
            sourceCD?.link = source.link?.absoluteString ?? ""
            sourceCD?.date = sourceCD?.date ?? NSDate()
            sourceCD?.feedItems = self.save(source.items, at: context, in: sourceCD)
            sourceCD?.isFavorite = sourceCD?.isFavorite ?? false
            
            context.mr_saveToPersistentStoreAndWait()
            
        }) { (success, error) in
            log.error(error?.localizedDescription)
//           self.logAllSource()
        }
    }
    
    private func sourceBy(_ attribute: String, with value: String, in context: NSManagedObjectContext) -> SourceCD?{
        if let originalSource = SourceCD.mr_findFirst(byAttribute: attribute, withValue: value, in: context){
            return originalSource
        }
        return SourceCD.mr_createEntity(in: context)
    }
    
    private func save(_ feeds: [FeedItem], at context: NSManagedObjectContext, in sourceCD: SourceCD?) -> NSMutableSet{
        return NSMutableSet(array:
            feeds.flatMap({
            let feedCD = feedItemBy("title", with: $0.title, in: context)
            feedCD?.title = $0.title
            feedCD?.text = $0.text
            feedCD?.image = $0.image?.absoluteString
            feedCD?.date = $0.pubDate as NSDate?
            feedCD?.link = $0.link?.absoluteString
            feedCD?.source = sourceCD
            return feedCD
        }))
    }
    
    private func feedItemBy(_ attribute: String, with value: String, in context: NSManagedObjectContext) -> FeedItemCD?{
         if let originalFeedCD = FeedItemCD.mr_findFirst(byAttribute: attribute, withValue: value, in: context){
            return originalFeedCD
        }
        return FeedItemCD.mr_createEntity(in: context)
    }
    
    fileprivate func logAllSource(){
        let sources = SourceCD.mr_findAll()
        for source in sources!{
            let s = source as! SourceCD
            log.info(s.name ?? "пусто")
            log.info(s.isFavorite)
            for item in s.feedItems! {
                log.info( (item as! FeedItemCD).title ?? "пусто")
            }
        }

    }
}

extension SourceServiceData{
    func add(link: String,
             complete:@escaping () -> Void,
             failed:@escaping (DataServiceError) -> Void){
        
        if isExist(link){
            failed(.sourceError("Этот линк уже существует"))
            return
        }
        
        MagicalRecord.save({ (context) in
            if let sourceCD = SourceCD.mr_createEntity(in: context){
                sourceCD.link = link
                sourceCD.date = NSDate()
            }
            context.mr_saveToPersistentStoreAndWait()
            
        }) { (success, error) in
            
            let s = SourceCD.mr_findAll()
            log.info(s)
            complete()
        }
    }
    
    private func isExist(_ link: String) -> Bool{
        if (SourceCD.mr_findFirst(byAttribute: "link", withValue: link) != nil){
            return true
        }
        return false
    }
}

extension SourceServiceData{
    func remove(_ sourceCD: SourceCD){
        MagicalRecord.save({ (context) in
            sourceCD.mr_deleteEntity(in: context)
            context.mr_saveToPersistentStoreAndWait()
        })
    }
}

extension SourceServiceData{
    func links() -> [SourceCD]?{
        return SourceCD.mr_findAll() as? [SourceCD]
    }
}

extension SourceServiceData{
    func changeFavorite(_ sourceCD: SourceCD){
        MagicalRecord.save({ (context) in
            let src = sourceCD.mr_(in: context)
            src?.isFavorite = !(sourceCD.isFavorite)
            context.mr_saveToPersistentStoreAndWait()
            
        }) { (complete, error) in
            log.info(error?.localizedDescription)
        }
    }
}
